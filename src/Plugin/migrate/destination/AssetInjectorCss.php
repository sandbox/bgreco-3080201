<?php

namespace Drupal\asset_injector\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\EntityConfigBase;
use Drupal\migrate\Row;
use Drupal\asset_injector\Entity\AssetInjectorCss as AssetInjectorCssEntity;

/**
 * Destination for CSS rules. Deletes existing rules with the same machine name.
 *
 * @MigrateDestination(
 *   id = "entity:asset_injector_css"
 * )
 */
class AssetInjectorCss extends EntityConfigBase {

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $id = $row->getDestinationProperty('id');
    if ($existing_entity = AssetInjectorCssEntity::load($id)) {
      $existing_entity->delete();
    }
    return parent::import($row, $old_destination_id_values);
  }

}
