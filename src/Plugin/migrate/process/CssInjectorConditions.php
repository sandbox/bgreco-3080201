<?php

namespace Drupal\asset_injector\Plugin\migrate\process;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Migrates CSS Injector rule conditions to their Drupal 8 format.
 *
 * @MigrateProcessPlugin(
 *   id = "css_injector_conditions"
 * )
 */
class CssInjectorConditions extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ThemeHandlerInterface $theme_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($rule_type, $pages, $themes) = $value;

    $conditions = [];

    // Migrate page visibility conditions.
    if ($pages) {
      // Skip PHP rules (rule type CSS_INJECTOR_PHP in D7).
      if ($rule_type == 2) {
        throw new MigrateSkipRowException(sprintf("The CSS injector rule '%s' will have no PHP visibility conditions.", $row->getSourceProperty('title')));
      }

      // Migrate paths in the same way as block visibility is handled.
      $paths = preg_split("(\r\n?|\n)", $pages);
      foreach ($paths as $key => $path) {
        $paths[$key] = $path === '<front>' ? $path : '/' . ltrim($path, '/');
      }
      $conditions['request_path'] = [
        'id' => 'request_path',
        'negate' => !$rule_type,
        'pages' => implode("\n", $paths),
      ];
    }

    // Migrate theme conditions, including only themes that exist on the
    // destination site.
    $installed_themes = $this->themeHandler->listInfo();
    $themes = unserialize($themes);
    $themes = array_intersect_key($themes, $installed_themes);
    if (!empty($themes)) {
      $conditions['current_theme'] = [
        'id' => 'current_theme',
        'theme' => $themes,
      ];
    }

    return $conditions;
  }

}
