<?php

namespace Drupal\asset_injector\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * CSS injector rule from database.
 *
 * @MigrateSource(
 *   id = "css_injector_rule",
 *   source_module = "css_injector"
 * )
 */
class CssInjectorRule extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('css_injector_rule', 'cir')->fields('cir');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'crid' => $this->t('The primary identifier for the CSS injection rule.'),
      'title' => $this->t('The descriptive title of the CSS injection rule.'),
      'rule_type' => $this->t('The type of rule to use when determining if the CSS should be injected.'),
      'rule_conditions' => $this->t('The data to evaluate when determining if the CSS should be injected.'),
      'rule_themes' => $this->t('Themes that CSS rule will be applied to.'),
      'media' => $this->t('The media type of the CSS file (screen, print, etc.).'),
      'preprocess' => $this->t('Whether the CSS file should be included by the CSS preprocessor.'),
      'enabled' => $this->t('Whether the rules should be enabled.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['crid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   *
   * CSS code is not stored in the database on the source site, so the code must
   * be read directly from the public files.
   */
  public function prepareRow(Row $row) {
    // Check if the source_base_path was provided by the migration.
    $source_base_path = $this->migration->getSourceConfiguration()['constants']['source_base_path'];
    if (empty(trim($source_base_path, '/'))) {
      $message = 'CSS injector rule was not migrated because the location of the source files was not provided.';
      $this->idMap->saveMessage($row->getSourceIdValues(), $message, MigrationInterface::MESSAGE_WARNING);
      return FALSE;
    }

    // Construct the path to the source file, relative to the site root.
    $source_public_path = $this->variableGet('file_public_path', 'sites/default/files');
    $rule_id = $row->getSourceProperty('crid');
    $file_path = "$source_base_path/$source_public_path/css_injector/css_injector_$rule_id.css";
    // Try to read the CSS code.
    $code = @file_get_contents($file_path);
    if ($code === FALSE) {
      $message = 'CSS injector rule ' . $row->getSourceProperty('title') . ' was not migrated because the CSS could not be read from the file: ' . $file_path;
      $this->idMap->saveMessage($row->getSourceIdValues(), $message, MigrationInterface::MESSAGE_WARNING);
      return FALSE;
    }

    // Store the CSS code in the source property.
    $row->setSourceProperty('code', $code);
    return parent::prepareRow($row);
  }

}
